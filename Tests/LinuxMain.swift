import XCTest
@testable import Run

XCTMain([
     testCase(Run.allTests)
])
